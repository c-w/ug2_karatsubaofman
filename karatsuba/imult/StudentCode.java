package imult;

import java.io.File;
import java.util.Vector;

public class StudentCode {
    /**
     * Returns the result of the addition of two BigInt objects.<p>
     * Terminates in O(n) time.
     * @param A operand 1
     * @param B operand 2
     * @return A+B
     * @see imult.BigInt
     */
    public static BigInt add(BigInt A, BigInt B) {
        int maxLen = Math.max(A.length(), B.length());
        BigInt result = new BigInt();
        DigitCarry dc = new DigitCarry();
        
        for (int i = 0; i < maxLen; i++) {
            dc = Arithmetic.addDigits(A.getDigit(i), B.getDigit(i), dc.carry());
            result.setDigit(i, dc.digit());
            result.setDigit(i+1, dc.carry());
        }
        
        return result;
    }
    
    /**
     * Returns the result of the subtraction of two BigInt objects.<p>
     * Terminates in O(n) time.
     * @param A operand 1
     * @param B operand 2
     * @return A-B
     * @see imult.BigInt
     */
    public static BigInt sub(BigInt A, BigInt B) {
        int maxLen = Math.max(A.length(), B.length());
        BigInt result = new BigInt();
        DigitCarry dc = new DigitCarry();
        
        for (int i = 0; i < maxLen; i++) {
            dc = Arithmetic.subDigits(A.getDigit(i), B.getDigit(i), dc.carry());
            result.setDigit(i, dc.digit());
            result.setDigit(i+1, dc.carry());
        }
        
        return result;
    }
    
    /**
     * Returns the result of the multiplication of two BigInt objects.<p>
     * Uses Karatsuba-Ofman to compute the result in O(n^lg(3)) time.
     * @param A operand 1
     * @param B operand 2
     * @return A*B
     * @see imult.BigInt
     */
    public static BigInt koMul(BigInt A, BigInt B) {
        // base case: return result of multiplication of A and B directly
        if (A.length() <= 1 && B.length() <= 1) {
            DigitCarry dc = Arithmetic.mulDigits(A.getDigit(0), B.getDigit(0));
            
            Vector<Unsigned> ivals = new Vector<Unsigned>();
            ivals.add(dc.digit());
            ivals.add(dc.carry());
            
            return new BigInt(ivals);
        }
        
        int maxLen = Math.max(A.length(), B.length());
        int lower = (int) Math.floor(maxLen/2.0);
        
        // recursive step: divide...
        BigInt a0 = A.split(0, lower-1);
        BigInt a1 = A.split(lower, maxLen);
        BigInt b0 = B.split(0, lower-1);
        BigInt b1 = B.split(lower, maxLen);
        BigInt l = koMul(a0, b0);
        BigInt h = koMul(a1, b1);
        BigInt m = sub(sub(koMul(add(a0, a1), add(b0, b1)), l), h);
        m.lshift(lower); // m = m B^floor(n/2)
        h.lshift(2*lower); // h = h B^(2*floor(n/2))
        
        // ...and conquer
        return add(l, add(m, h));
    }
    
    /**
     * Returns the result of the multiplication of two BigInt objects.<p>
     * Uses a hybrid between Karatsuba-Ofman and School-Style multiplication:
     * If the shortest of the operands has fewer than 10 digits, use
       {@link Arithmetic#schoolMul} to return the answer in O(n^2) time.
     * Otherwise use Karatsuba-Ofman to divide (and conquer) the problem.
     * @param A operand 1
     * @param B operand 2
     * @return A*B
     * @see imult.BigInt
     */
    public static BigInt koMulOpt(BigInt A, BigInt B) {
        // base case: return result of multiplication of A and B directly
        if (!(Math.min(A.length(), B.length()) > 10)) {
            return Arithmetic.schoolMul(A, B);
        }
        
        int maxLen = Math.max(A.length(), B.length());
        int lower = (int) Math.floor(maxLen/2.0);
        
        // recursive step: divide...
        BigInt a0 = A.split(0, lower-1);
        BigInt a1 = A.split(lower, maxLen);
        BigInt b0 = B.split(0, lower-1);
        BigInt b1 = B.split(lower, maxLen);
        BigInt l = koMulOpt(a0, b0);
        BigInt h = koMulOpt(a1, b1);
        BigInt m = sub(sub(koMulOpt(add(a0, a1), add(b0, b1)), l), h);
        m.lshift(lower); // m = m B
        h.lshift(2*lower); // h = h B^2
        
        // ...and conquer
        return add(l, add(m, h));
    }
    
    public static void main(String argv[]) {
        /*
        // Unit test: add(BigInt A, BigInt B)
        BigInt A = new BigInt("645 123 000 012 556");
        BigInt B = new BigInt(" 77 543   0 012 376");
        String add_AB_Actual = String.valueOf(add(A, B).value());
        String add_AB_Expected = "7220666000000240932";
        if (!add_AB_Actual.equals(add_AB_Expected)) {
            System.out.printf("***** Problem with add() *****\n");
            System.out.printf("Got: %s\n", add_AB_Actual);
            System.out.printf("Expected: %s\n", add_AB_Expected);
            System.exit(1);
        }
        
        // Unit test: sub(BigInt A, BigInt B)
        String sub_AB_Actual = String.valueOf(sub(A, B).value());
        String sub_AB_Expected = "5679580000000000180";
        if (!sub_AB_Actual.equals(sub_AB_Expected)) {
            System.out.printf("***** Problem with sub() *****\n");
            System.out.printf("Got: %s\n", sub_AB_Actual);
            System.out.printf("Expected: %s\n", sub_AB_Expected);
            System.exit(1);
        }
        
        // Unit test: koMul(BigInt A, BigInt B)
        BigIntMul.mulTest(new Unsigned(10), new Unsigned(10));
        BigIntMul.mulTest(new Unsigned(10), new Unsigned(100));
        BigIntMul.mulTest(new Unsigned(10), new Unsigned(1000));
        */
        
        Unsigned m = new Unsigned(1);
        Unsigned n = new Unsigned(10);
        Unsigned t = new Unsigned(90);
        boolean opt = true;
        File f = new File(String.format("koMul%sTimes", opt ? "Opt" : ""));
        BigIntMul.getRunTimes(m, n, t, f, opt);
        
        File fout = new File("ratios");
        Unsigned xOver = new Unsigned(130);
        BigIntMul.getRatios(m, n, t, fout, xOver);
        
        float c = 0.0011901146366959873f;
        float a = 9.495971676986242E-4f;
        BigIntMul.plotRunTimes(c, a, f);
    }
}
